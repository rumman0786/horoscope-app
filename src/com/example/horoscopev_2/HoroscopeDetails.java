package com.example.horoscopev_2;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class HoroscopeDetails extends Activity {
	TextView hLifePathNumber, hExpressionNumber, hDailyNumber;
	int nLifePathNumber = 0,nExpressionNumber = 0,nDailyNumber = 0   ; 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.detailed_meaning);
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			String sLifePathNumber = extras.getString("lifePathNumber");
			String sExpressionNumber = extras.getString("expressionNumber");
			String sDailyNumber = extras.getString("dailyNumber");
			
			initialize();
			
			nLifePathNumber = Integer.parseInt(sLifePathNumber);
			nExpressionNumber = Integer.parseInt(sExpressionNumber );
			nDailyNumber  = Integer.parseInt(sDailyNumber);
			
			showMessages();
		}
	}
	private void initialize() {
		hExpressionNumber = (TextView) findViewById(R.id.tvExpressionNumberMessage);
		hLifePathNumber = (TextView) findViewById(R.id.tvLifePathNumberMessage);
		hDailyNumber = (TextView) findViewById(R.id.tvDailyNumberMessage);
		
	}
	private void showMessages() {
		Jotishi j = new Jotishi();
		Log.d("Exp", ""+nExpressionNumber);
		Log.d("lp", ""+nLifePathNumber);
		Log.d("dp", ""+nDailyNumber);
		Log.d("m1", ""+j.predictionExpressionNumber(nExpressionNumber));
		Log.d("m2", ""+j.predictionLifePathNumber(nLifePathNumber));
		Log.d("m3", ""+j.predictionDailyNumber(nDailyNumber));
		hExpressionNumber.setText("Meaning of Expression number:\n"+j.predictionExpressionNumber(nExpressionNumber)); 
		hLifePathNumber.setText("Meaning of Life Path number:\n"+j.predictionLifePathNumber(nLifePathNumber));
		hDailyNumber.setText("Meaning of Daily number:\n"+j.predictionDailyNumber(nDailyNumber));
	}
}
