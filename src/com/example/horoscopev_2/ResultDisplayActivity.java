package com.example.horoscopev_2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class ResultDisplayActivity extends Activity implements OnClickListener {
	TextView rLifePthNumber,rExpressionNumber,rDailyNumber;
	private String lifePathNumber,expressionNumber,dailyNumber;
	private Button bDetails;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.resultshow);
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
		    lifePathNumber = extras.getString("lifePathNumber");
		    expressionNumber = extras.getString("expressionNumber");
		    dailyNumber = extras.getString("dailyNumber");
		    
		    rLifePthNumber = (TextView) findViewById(R.id.tvLifePathNumber);
		    rExpressionNumber = (TextView) findViewById(R.id.tvExpressionNumber);
		    rDailyNumber= (TextView) findViewById(R.id.tvDailyNumber);
		    
		    rLifePthNumber.setText("Life Path Number : "+ lifePathNumber);
		    rExpressionNumber.setText("Your Expression Number: "+expressionNumber);
		    rDailyNumber.setText("Your Daily number : "+dailyNumber);
		    
		    bDetails = (Button) findViewById(R.id.button1);
		    bDetails.setOnClickListener(this);
		}
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent i = new Intent(getApplicationContext(), HoroscopeDetails.class);
		i.putExtra("lifePathNumber",lifePathNumber);
		i.putExtra("expressionNumber",expressionNumber);
		i.putExtra("dailyNumber",dailyNumber);
		startActivity(i);
	}
}
