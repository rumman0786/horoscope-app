package com.example.horoscopev_2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener{
	
	private DatePicker birthDate;
	private Button btnCalculate;
	private RadioGroup radioSexGroup;
	private RadioButton radioSexButton;
	private EditText mEtname;
	private int year;
	private int month;
	private int day;
	private Jotishi jotishi;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		intialize();
		jotishi = new Jotishi();
	}
	private void intialize() {
		// TODO Auto-generated method stub
		//tvDateTest = (TextView) findViewById(R.id.tvDateResult);
		birthDate = (DatePicker) findViewById(R.id.datePicker);
		btnCalculate = (Button) findViewById(R.id.bCalculate);
		radioSexGroup = (RadioGroup) findViewById(R.id.radioGroupGender);
		mEtname = (EditText) findViewById(R.id.etName); 
		
		btnCalculate.setOnClickListener(this);
		//birthDate.
	}
	@Override
	public void onClick(View v) {
		// Calendar

		this.day = birthDate.getDayOfMonth(); 
		this.month = birthDate.getMonth()+1;
		this.year = birthDate.getYear();
		
		String lifePathNumber = jotishi.completeDateWork(day+"-"+month+"-"+year);
		String expressionNumber = jotishi.completeNameWork(mEtname.getText().toString());
		String dailyNumber = jotishi.getDailyNumber(day+"-"+month);
		int selectedId = radioSexGroup.getCheckedRadioButtonId();
		radioSexButton = (RadioButton) findViewById(selectedId);
		Toast.makeText(MainActivity.this,
				radioSexButton.getText(), Toast.LENGTH_SHORT).show();
		//tvDateTest.setText(lifePathNumber+ "\n"+expressionNumber+"\n"+dailyNumber);
		
		Intent i = new Intent(getApplicationContext(), ResultDisplayActivity.class);
		i.putExtra("lifePathNumber",lifePathNumber);
		i.putExtra("expressionNumber",expressionNumber);
		i.putExtra("dailyNumber",dailyNumber);
		startActivity(i);
		
		
	}

}
