package com.example.horoscopev_2;

import java.util.Calendar;

public class Jotishi {

	private String[] expressionNumberPredictions = {
			"A person with Life Path number 1 is hard working, a natural born leader, has a pioneering spirit that is full of energy, and a passion for art. They have a strong desire to be number one, which means a person with this number can manifest very easily. Due to their determination and self motivation, they won't let anything stand in their way of accomplishing a goal. Their drive allows them to overcome any obstacle or challenge they may encounter, and they have the desire to accomplish great things in their lifetime. Their only need is to focus on what they want in order to achieve it.Because Ones are critical of themselves, they can also be very critical of others. The Ones will not tolerate laziness from the people they spend their time with.People with Life Path 1 are very independent and feel the need to make up their own mind about things and follow their own personal convictions. All this drive and determination means that they can easily become irritated when things don't go their way.",
			"A person with Life Path number 2 seeks harmony and peace, and is symbolized by relationships, co-operation, and being considerate and thoughtful of others. People with a Life Path 2 are natural peacemakers, and because they see all the viewpoints in any situation, handle difficult situations with grace, and tend to be persuasive rather than forceful when trying to get their point across, people may often look to them to be a mediator in any argument. They are very loyal, and when they say that they love you, count on it! They welcome companionship and the chance to share their lives with someone special. They are extremely sensitive to others and have the ability to truly listen. Twos are sincere, honest and open and see the best in people. Because of their sensitivity, gentleness and loving spirit, they make an excellent friend or lover.",
			"People with a Life Path number 3 have a very high level of creativity and self expression. This abundance of creative energy, and the ease with which they are able to communicate in all areas, both written word and verbal, could lead them to become a poet, actor, writer, artist or musician. In fact many writers, radio broadcasters, actors, singers, performers, and counselors share this life path number. Threes are optimistic, extremely generous and giving souls, and are able to find positive in everything around them. People like to be around them, not only because of these qualities, but also because Threes have a charismatic personality, are great listeners and are very conscious of other people's feelings and emotions. They can easily put the people around them at ease and make them feel comfortable. Because they enjoy living life to the fullest, Threes tend to live life for today and not worry about tomorrow. They have a hard time taking responsibilities seriously, and probably aren't very good with money, partly because they feel so positive about life they figure everything will work itself out fine. This can sometimes lead those with a Life Path of 3 to live superficially, have a lack of direction in their life, and procrastinate. When they are hurt emotionally, Threes tend to withdraw and become moody, and can sometimes make biting comments to lash out at people around them. They can be manic depressive if they do not use their creative energy and tend to exaggerate the truth.",
			"People with a Life Path number 4 are the worker bees of society. If your Life Path is a 4 you are determined, practical and hard working. Down-to-earth is a term that is probably often used to describe you. You find hard work rewarding and don't look for the easy way to the top or to finding success. Not only do you work hard yourself, but you expect the same from those around you. The Fours like to be organized, and to put things back in their proper place it is one of their strong points, and they feel better able to tackle challenges if they have a solid plan in place beforehand. They tend to be set in their ways and are drawn to leading an orderly life ie. a place for everything and everything in its place. Home is their haven, and if their home environment appears sloppy and unkempt, that is a sign that a Life Path 4 person is not doing well. They are usually very cerebral and need to find ways to relax their minds. Otherwise, great ideas live and die in their heads. They have a strong sense of right and wrong, are very honest, and value honesty in others. Fours dreams are based in reality and they never question that you will have to work hard to make them come true. Loyal and very dependable, they make an excellent friend or partner, but may have just a small circle of friends.",
			"Those with a Life Path of 5 seek freedom above all else. They are adventurers, having a restless nature, and being on the go, constantly seeking change and variety in life. They have a free spirit and need to have variety in their day. If they do not live the adventure, their lives become way too dramatic. They love meeting new people, trying new things, and living life for today, and curiosity leads them to constantly try to find the answers to life's questions. Conservative is a word that is probably never used to describe them, as they love taking risks, and hate routine and repetition.",
			"Those born with a Life Path number 6 are incredible nurturers. If men, they rescue damsels in distress. If women, they mother the little boy in their men.If you have a six in your chart you are home, family, or community oriented, loving, warm, understanding, compassionate, responsible and reliable and interested in pleasing others. You are an excellent caretaker and provider, and enjoy being of service to others, and this is especially true with your family and friends. You life revolves around home and family, and your parenting instincts are very strong. The word domestic most likely describes you well, and one job you would love is being a stay at home parent.",
			"Seven is another cerebral number, and those with a Life Path number 7 have a loner quality. They need to learn to have faith. If they do not have faith they tend to become very cynical and escape through drugs, alcohol, work, and geography. They have a love of natural beauty: ocean, green grass, plants, flowers, etc.Sevens have an air of mystery and do not want you to know who they are. Intellectual, analytical, intuitive, reserved, natural inclination towards spiritual subjects, aloof, loner, pessimistic, secretive, and insecure; are some of the qualities of those born into the Seventh Life path. A person who is a Life Path 7 is a thinker. If your Life Path is a 7 you are wise and studious. You seek truth and wisdom in all that you do, and search for the underlying answers in everything. Your tendency is to be a perfectionist, and you expect the same from those around you.",
			"People with a Life Path number 8 do not feel safe unless they have found a way to establish financial security. It is difficult for an Eight to take advice. When they make a choice, they must feel it is their decision, NOT SOMEONE ELSES. As a result, they do tend to learn the hard way. Eights are very honest and by being so blunt, they unintentionally hurt feelings. Although they can sometimes appear insensitive, what is going on inside them is the exact opposite. They do feel deeply about everything that goes on in their lives. People with a Life Path 8 are born with natural leadership skills. If your Life Path is an 8 you are very ambitious and goal oriented. You have strong organizational skills and broad vision which make you successful in business.",
			"Those born with a Life Path number 9 are natural leaders, and they assume they are in charge even if they are not. If in a department store, people think they work there. They take care of everyone else but need to learn to speak up when they need help, love, and hugs. Nines often feel unloved or abandoned by their mother or father, or they feel completely responsible for them. It's hard for them to let go of the past. People with a Life Path 9 are humanitarians. If your Life Path is a 9 you have an extremely strong sense of compassion and generosity. You are selfless and helping others is very important to you. You not only want to help others, but you feel very deeply for those less fortunate than yourself. You are friendly and people like you. Your generosity knows no bounds, and you give freely of your money, time and energy. Your ultimate goal is working toward a better world." };

	private String[] lifePathNumberPredictions = {
			"Postitive Traits : A person with positive 1 traits abounds in creative inspiration, and possesses the enthusiasm and drive to accomplish a great deal. Your drive and potential for action comes directly from the enormous depth of strength you have. This is both the physical and inner varieties of strength. With this strength comes utter determination and the capability to lead. As a natural leader you have a flair for taking charge of any situation. Highly original, you may have talents as an inventor or innovator of some sort. In any work that you choose, your independent attitude can show through. You have very strong personal needs and desires, and you feel it is always necessary to follow your own convictions. You are ambitious, and either understand or must learn the need for aggressive action to promote yourself. Although you may hide the fact for social reasons, you are highly self-centered and demand to have your way in most circumstances.\n\nNegative Traits : When the 1 Life Path person is not fully developed and expressing the negative side of this number, the demeanor may appear very dependent rather than independent. If you are expressing this negative trait of the number 1, you are likely to be very dissatisfied with your circumstances, and long for self-sufficiency. This is defined as the weak or dependent side of the negative 1 Life Path. On the strong side of this negative curve, the 1 energy can become too self-serving, selfish and egotistical. Over-confidence and impatience mark this individual.",
			"Postitive Traits : You are totally honest and open in thought, word and deed. You are apt to excel in any for of group activity where your expertise in handling and blending people can be used effectively. Manners and tact mark your way with others at all times, and you are not one to dominate a group or situation. You are the master of compromise and of maintaining harmony in your environment, never stooping to aggravate or argue.\n\nNegative Traits : The negative side of the 2 is hardly a problem. The biggest obstacle and difficulty you may face is that of passivity and a state of apathy and lethargy. The negative 2 can be very pessimistic and accomplishes very little. Needless to say, the negative 2 doesn't belong in the business world and even the more positive individual with the 2 Life Path may prefer a more amiable and less competitive environment.",
			"Postitive Traits : The bright side of this path stresses harmony, beauty and pleasures; of sharing your creative talents with the world. Capturing your capability in creative self-expression is the highest level of attainment for this life path. The characteristics of the 3 are warmth and friendliness, a good conversationalist, social and open. The approach to life tends to be exceedingly positive, however, and your disposition is almost surely sunny and open-hearted. You effectively cope with all of the many setbacks that occur in life and readily bounce back for more.You have good manners and seem to be very conscious of other people's feelings and emotions. Life is generally lived to the fullest, often without much worry about tomorrow. You are not very good at handling money because of a general lack of concern about it. You spend it when you have it and don't when you don't.\n\nNegative Traits : On the negative side, a 3 may be so delighted with the joy of living that the life becomes frivolous and superficial. You may scatter your abilities and express little sense of purpose. The 3 can be an enigma, for no apparent reason you may become moody and tend to retreat. Escapist tendencies are not uncommon with the 3 life path, and you find it very hard to settle into one place or one position. Guard against being critical of others, impatient, intolerant, or overly optimistic.",
			"Postitive Traits : You are an excellent organizer and planner because of your innate ability to view things in a very common sense and practical way. You are a wonderful manager with a great sense of how to get the job done.Loyal and devoted, you make the best of your marriage, and you are a dependable business partner. Friends may be few in number, but you are very close to them and once friendships are made, they often last a lifetime. The number 4 is solidly associated with the element of earth from which it gains it strength and utter sense of reality. You are one of the most dependable people you know. If patience and determination can ever win, you are sure to achieve great success in life.\n\nNegative Traits : The negative side of the 4 can prove dogmatic to an excess, narrow-minded, and repressive. A lot of skin-deep people turn you off, and you lack the tact to keep your feelings from being totally clear to all around. Additionally, the negative 4 has a bad tendency to get too caught up in the daily routine of affairs and often misses the big picture and major opportunities that come along once in a while.",
			"Postitive Traits : The number 5 personality is rather happy-go-lucky; living for today, and not worrying too much about tomorrow. It is important for you to mix with people of a like mind, and try to avoid those that are too serious and demanding. It is also important for you to find a job that provides thought-provoking tasks rather than routine and redundant responsibilities. You do best dealing with people, but the important thing is that you have the freedom to express yourself at all times. You have an innate ability to think through complex matters and analyze them quickly, but then be off to something new.\n\nNegative Traits : In the most negative application or use of the 5 energies, you could become very irresponsible in tasks and decisions concerning your home and business life. The total pursuit of sensation and adventure can result in your becoming self-indulgent and totally unaware of the feelings of those around you. In the worse case situations negative 5's are very undependable and self-serving.",
			"Postitive Traits : Most with Life Path 6 are the positive types who willingly carry far more than their fair share of the load and are always there when needed. You are very human and realistic about life, and you feel that the most important thing in your life is the home, family and friends.\n\nNegative Traits : Avoid a tendency to become overwhelmed by responsibilities and a slave to others. The misuse of this Life Path produces tendencies towards exaggeration, over-expansiveness, and self-righteousness. Imposing one's views in an interfering or meddling way must be an issue of concern. The natural burdens of this number are heavy, and on rare occasions, responsibility is abdicated by persons with this Life Path 6. This rejection of responsibility will make you feel very guilty and uneasy, and it will have very damaging effects upon your relationships with others.",
			"Postitive Traits : You are very thorough and complete in your work, the perfectionist who expects everyone else to be a meet a high standard of performance, too. You evaluate situations very quickly and with amazing accuracy. . You aren't one to have a wide circle of friends, but once you accept someone as a friend, it's for life. You really aren't a very social person, and your reserve is often taken to be aloofness. Actually, it's not that at all, but merely a cover up for your basic feeling of insecurity. You actually like being alone, away from the hustle and bustle of modern life.\n\nNegative Traits : In the most negative use of the 7 energies, you can become very pessimistic, lackadaisical, quarrelsome, and secretive. A Life Path 7 individual who is not living life fully and gaining through experiences, is a hard person to live with because of a serious lack of consideration and because there is such a negative attitude. The negative 7 is very selfish and spoiled. If you have any of the negative traits they are very difficult to get rid of because you tend to feel that the world really does owe you a living or in some way is not being fairly treated. Fortunately, the negative 7 is not the typical 7, at least not without some mitigating positive traits. This number is one that seems to have some major shifts from highs to lows. Stability in feelings may be elusive for you.",
			"Postitive Traits : If you are a positive 8 you are endowed with tremendous potential for conceiving far-reaching schemes and ideas, and also possessing the tenacity and independence to follow them through to completion. In short, you are well-equipped for competition in the business world or in other competitive fields of endeavor. You know how to manage yourself and your environment. You are practical and steady in your pursuit of major objectives, and you have the courage of your convictions when it comes to taking the necessary chances to get ahead.\n\nNegative Traits : The negative 8 can be dictatorial and often suppresses the enthusiasm and efforts of fellow member of the environment. Often, the strength of their own personality excludes close feelings for other people with whom they come in contact. Material gains and rewards often become issues of utmost importance, even to the neglect of family, home and peace of mind. Dedication to success can become an obsession. Emotional feelings are often suppressed by the negative 8, resulting in isolation and loneliness. All Life Path 8 people must avoid discounting the opinions of others.",
			"Postitive Traits : Material gains are not overly important, although the quality of some life path 9 people is such that they are materially rewarded in very significant ways. Often, the number 9 life path requires a very selfless attitude and the giving up of material possessions for the common good. Even the very average of those with life path 9 possess extremely compassionate tendencies. The desire to help others, especially the troubled or underprivileged, is strong. You are apt to frequently find yourself being used and let down by others, as your generosity is misused and abused.The number 9's very deep understanding of life is sometimes manifested in the artistic and literary fields .You have the ability to make friends very easily, as people are attracted to your magnetic, open personality.\n\nNegative Traits : As do all the life path numbers, the 9 has its negative side, and because of the demanding nature of the truly positive 9, many tend to fail in this category. It is not uncommon for persons with the 9 life path to fight the realities and challenges of purpose imposed here because selflessness is not an easy trait. You may have difficulty believing that giving and a lack of personal ambition can be satisfying. It must be realized and accepted that little long-term satisfaction and happiness is to be gained by rejecting the natural humanitarian inclinations of this path.", };
	private String[] dailyNumberPredictions = {
			"Starting something is more likely today than on days with other numbers. An urge for independence or exploration may be experienced. Leading others is likely to feel more natural.",
			"Teamwork is likely to be required in order to make progress with something. Your consideration of others is required. Relationships seem more important.",
			"The number 3 personal day tends to be a relatively joyful day. Expressing yourself feels easier. Art is more appreciated. The idea of socializing is appealing.",
			"Home feels like the right place to be. It may be a day for concentrating on work or a day leisurely exploring nature. Family feels extra important. You're likely to find yourself organizing something and having some attention on planning for the future.",
			"Today you feel like expressing your personal freedom. You are happiest around other people. Today you tend more than usual to talk without thinking, to speak your mind; keep in mind that tact can be useful. Your energy is higher.",
			"A sense of personal responsibility and also responsibility for others is stronger today, especially responsibility for family members. Harmony in your environment seems more important. Writing can be easier today. You tend to be more careful with money, unless it involves a family member, in which case you tend to be more generous.",
			"The number 7 personal day is a day for studying. Also a day for meditating. It is easier to find answers within. There is greater awareness and understanding of others. You may also be more critical and more inclined to demand perfection.",
			"It is a day of practicality. Ambition and drive are enhanced today. Goals are clearer. You have the ability to be charming, especially if it helps you get your way. The supernatural may intrigue.",
			"There is less selfishness today, a more giving and forgiving nature. Creativity is enhanced. There is a desire to perform humanitarian deeds. Also a tendency to become involved in spiritual, philosophical, or educational issues. Recognition may come your way today." };

	public String completeDateWork(String date) {
		// TODO Auto-generated method stub
		int lifePathNumber = 0;
		date = date.replace("-", "");
		while ((lifePathNumber = getReducedNumber(Integer.parseInt(date))) > 9) {
			date = "" + lifePathNumber;
		}
		return "" + lifePathNumber;

	}

	public String completeNameWork(String fullName) {
		int fractionalExpressionNumber = 0, temp, recalculatedNumber = 0, expressionNumber = 0;
		// Scanner sc = new Scanner(System.in);
		// String fullName = sc.nextLine();
		System.out.println(fullName);
		String[] nameFractions = fullName.split(" ");
		for (String s : nameFractions) {
			for (int i = 0; i < s.length(); i++) {
				char c = s.charAt(i);
				temp = (c % 64) % 9;
				System.out.println(c + " : " + temp);
				fractionalExpressionNumber += temp;
			}
			recalculatedNumber = getReducedNumber(fractionalExpressionNumber);
			System.out.println(s + " : " + recalculatedNumber);
			expressionNumber += recalculatedNumber;
		}
		return "" + getReducedNumber(expressionNumber);

	}

	public String getDailyNumber(String date) {
		final Calendar c = Calendar.getInstance();
		int day = c.get(Calendar.DAY_OF_MONTH);
		int month = c.get(Calendar.MONTH) + 1;
		int year = c.get(Calendar.YEAR);
		String[] birthInfo = date.split("-");
		int bday = Integer.parseInt(birthInfo[0]);
		int bmonth = Integer.parseInt(birthInfo[1]);
		day = getReducedNumber(day);
		month = getReducedNumber(month);
		year = getReducedNumber(year);
		bday = getReducedNumber(bday);
		bmonth = getReducedNumber(bmonth);
		int sum = day + month + year + bday + bmonth;

		return "" + getReducedNumber(sum);
	}

	public String predictionExpressionNumber(int expressionNumber) {
		if (expressionNumber > 0 && expressionNumber <= 9) {
			return expressionNumberPredictions[expressionNumber - 1];
		}
		return "Vobisshot Ondhokar :P";
	}

	public String predictionLifePathNumber(int lifePathNumber) {
		if (lifePathNumber > 0 && lifePathNumber <= 9) {
			return lifePathNumberPredictions[lifePathNumber - 1];
		}
		return "Vobisshot Ondhokar :P";
	}

	public String predictionDailyNumber(int dailyNumber) {
		if (dailyNumber > 0 && dailyNumber <= 9) {
			return dailyNumberPredictions[dailyNumber - 1];
		}
		return "Vobisshot Ondhokar :P";
	}

	private int getReducedNumber(int expressionNumber) {
		// TODO Auto-generated method stub
		int recalculatedNumber = 0;
		if (expressionNumber > 9) {
			String numberToSplit = "" + expressionNumber;
			for (int i = 0; i < numberToSplit.length(); i++) {
				recalculatedNumber += Integer.parseInt(""
						+ numberToSplit.charAt(i));
			}
		} else {
			return expressionNumber;
		}
		return recalculatedNumber;
	}

}
